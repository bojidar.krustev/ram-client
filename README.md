## Rick and Morty episodes client


![](https://imgur.com/ousyQaC.png)

Built with React, Apollo and GraphQL

## Setup

Use the package manager [yarn](https://yarnpkg.com/cli/install) to install ram-client packages.

```bash
yarn
```

Start development environment. 

```bash
yarn start
```
